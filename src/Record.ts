namespace BookMarker {
  /**
   * 一个记录
   * @class
   */
  export class Record {
    /**
     * 创建一个记录
     * @param url 网址
     * @param title 网站标题
     */
    constructor(public url: string, public title: string) {}

    // 序列化
    toJSON() {
      return {
        url: this.url,
        title: this.title,
      }
    }
  }
}

export default BookMarker
