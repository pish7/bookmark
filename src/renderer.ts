import { shell } from 'electron'
import BookMarker from './Record'

/** Chromium 提供的用于解析响应文本的解析器 */
const parser = new DOMParser()

/** 列表 */
const linksSection = document.querySelector('.links') as HTMLElement
/** 出错消息 */
const errorMessage = document.querySelector('.error-message') as HTMLDivElement
/** 输入表单 */
const newLinkForm = document.querySelector('.new-link-form') as HTMLFormElement
/** URL 输入框 */
const newLinkUrl = document.querySelector('.new-link-url') as HTMLInputElement
/** 提交按钮 */
const newLinkSubmit = document.querySelector(
  '.new-link-submit'
) as HTMLInputElement
/** 清除按钮 */
const clearStorageButton = document.querySelector(
  '.clear-storage'
) as HTMLButtonElement

// 处理字符输入事件
newLinkUrl.addEventListener('keyup', () => {
  // 判断输入框的校验是否有效，以此决定提交按钮是否可用
  // 使用到 Chromium 的 ValidityState API
  newLinkSubmit.disabled = !newLinkUrl.validity.valid
})

// 处理表单提交事件
newLinkForm.addEventListener('submit', event => {
  event.preventDefault()

  const url = newLinkUrl.value

  fetch(url)
    .then(validateResponse)
    .then(response => response.text())
    .then(parseResponse)
    .then(findTitle)
    .then(title => storeLink(new BookMarker.Record(url, title)))
    .then(clearForm)
    .then(renderLinks)
    .catch(error => handleError(error, url))
})

// 处理清除按钮点击事件
// 清除全部记录
clearStorageButton.addEventListener('click', () => {
  localStorage.clear()
  linksSection.innerHTML = ''
})

// 处理整个列表的冒泡单击事件
linksSection.addEventListener('click', event => {
  const target = event.target as any
  if (target && target.href) {
    // 如果有 href 表明是一个带链接的 a 元素
    event.preventDefault()
    shell.openExternal(target.href)
  }
})

/**
 * 清空输入框
 */
function clearForm() {
  newLinkUrl.value = ''
}

/**
 * 解析网页内容
 * @param {string} text 网页源码
 * @return {Document} 解析后的 DOM 对象
 */
function parseResponse(text: string): Document {
  return parser.parseFromString(text, 'text/html')
}

/**
 * 获取 title 节点中的文本
 * @param {Document} nodes DOM 对象
 * @return {string} 网页 title 中的文本
 */
function findTitle(nodes: Document): string {
  const title = nodes.querySelector('title')
  if (title) {
    return title.innerText
  }
  return ''
}

/**
 * 保存一个链接到 localStorage 中
 * @param {BookMarker.Record} record 一条记录
 */
function storeLink(record: BookMarker.Record) {
  localStorage.setItem(record.url, JSON.stringify(record))
}

/**
 * 获取保存在 localStorage 中的所有记录
 * @return {Array.<BookMarker.Record>} 记录数组
 */
function getLinks(): BookMarker.Record[] {
  // 保存在 localStorage 中的所有键值对都可以直接在 localStorage 对象上访问
  return Object.keys(localStorage).map(key => {
    const record = JSON.parse(localStorage.getItem(key) as string)
    return new BookMarker.Record(record.url, record.title)
  })
}

/**
 * 将一条记录转换为一串 HTML 字符串
 * @param {BookMarker.Record} param0 记录对象
 * @return {string} 生成的 HTML 字符串
 */
function convertToElement({ title, url }: BookMarker.Record): string {
  return `<div class="link"><h3>${title}</h3>
          <p><a href="${url}">${url}</a></p></div>`
}

/**
 * 从 localStorage 中取出数据并渲染整个列表，直接将结果插入到页面 DOM 中
 */
function renderLinks() {
  const linkElements = getLinks()
    .map(convertToElement)
    .join('')
  linksSection.innerHTML = linkElements
}

/**
 * 链接地址无效时的错误处理程序
 * 生成错误消息显示在页面上，并在一定时间后自动清除
 * @param {Error} error 异常对象
 * @param {string} url 抛出异常的网站
 */
function handleError(error: Error, url: string) {
  errorMessage.innerHTML = `
    There was an issue adding "${url}": ${error.message}
  `.trim()
  setTimeout(() => (errorMessage.innerText = ''), 5000)
}

/**
 * 检验 HTTP 的响应是否正常
 * 如果正常，返回 HTTP 响应对象，否则抛出异常
 * @param {Response} response HTTP 响应对象
 * @return {Response} HTTP 响应对象
 */
function validateResponse(response: Response): Response {
  if (response.ok) {
    return response
  }
  // 如果响应的状态码属于 400 或 500 系列，response.ok 的值会是 false
  throw new Error(`Status code of ${response.status} ${response.statusText}`)
}

renderLinks()
