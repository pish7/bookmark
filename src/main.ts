import { app, BrowserWindow, Menu } from 'electron'

/**
 * 窗口句柄
 * @type {BrowserWindow|null}
 */
let win: BrowserWindow | null = null

/**
 * 当前环境是否为开发环境
 * @const
 * @type {boolean}
 */
const isDevelopment = process.env.NODE_ENV == 'development'

/**
 * 创建窗口
 */
function createWindow() {
  // 创建一个窗口
  win = new BrowserWindow({
    minWidth: 400,
    minHeight: 300,
    show: false,
    webPreferences: {
      nodeIntegration: true,
    },
  })

  // 装载界面文件
  win.loadFile(`${__dirname}/views/index.html`)

  // 窗口关闭
  win.on('closed', () => {
    win = null
  })

  // 渲染进程第一次完成绘制
  win.on('ready-to-show', () => {
    win && win.show()
  })
}

// 全部窗口关闭时程序退出
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('ready', () => {
  createWindow() // 创建窗口

  // 如果运行在开发环境中，则打开开发工具
  if (isDevelopment) {
    require('devtron').install() // 安装 devtron
    win && win.webContents.openDevTools() // 打开开发者工具
  } else {
    Menu.setApplicationMenu(null) // 移除菜单
  }
})
