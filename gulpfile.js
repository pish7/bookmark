const { series, parallel, src, dest, watch } = require('gulp')
const ts = require('gulp-typescript')
const tsProject = ts.createProject('./tsconfig.json')
const sass = require('gulp-sass')
const sourcemaps = require('gulp-sourcemaps')

sass.compiler = require('sass')

// 复制文件
exports.copyStatic = function copyStatic() {
  return src(['src/assets/**', 'src/views/**'], {
    base: './src',
  }).pipe(dest('app/'))
}

// 转译 TypeScript 到 JavaScript
exports.build_ts = function build_ts() {
  return src(['src/**/*.ts'], {
    base: './src',
  })
    .pipe(sourcemaps.init())
    .pipe(tsProject())
    .js.pipe(sourcemaps.write())
    .pipe(dest('app/'))
}

// 转译 scss 到 css
exports.build_scss = function build_scss() {
  return src(['src/**/*.scss'], {
    base: './src',
  })
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(dest('app/'))
}

// 定义默认任务
const defaultTask = series(this.copyStatic, this.build_ts, this.build_scss)
exports.default = defaultTask

// 定义监视任务
exports.auto = function auto() {
  return watch('src/**', { ignoreInitial: false }, defaultTask)
}
