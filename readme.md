# Electron 书签

## 运行效果

![运行效果](./bookmarker.gif)

## 初始化项目

### 创建远程仓库

### 创建本地仓库

```shell
mkdir electron-bookmarker
cd electron-bookmarker
git init
npm init -y
touch readme.md
git add .
git commit -m 初次提交
git remote add origin git@gitee.com:pish7/bookmark.git
git push -u origin master
```

## 搭建 typescript 和 sass 编译环境

### 安装 Gulp

```shell
npm i -D gulp-cli gulp gulp-sourcemaps
```

### 安装 typescript

```shell
npm i -D typescript gulp-typescript
```

### 安装 SASS

```shell
npm i -D sass gulp-sass
```

### 创建 typescript 配置文件

```shell
npx tsc --init
```

### 创建 gulp 配置文件

gulpfile.js

```js
const { series, parallel, src, dest, watch } = require('gulp')
const ts = require('gulp-typescript')
const tsProject = ts.createProject('./tsconfig.json')
const sass = require('gulp-sass')
const sourcemaps = require('gulp-sourcemaps')

sass.compiler = require('sass')

// 复制文件
exports.copyStatic = function copyStatic() {
  return src(['src/assets/**', 'src/views/**'], {
    base: './src',
  }).pipe(dest('app/'))
}

// 转译 TypeScript 到 JavaScript
exports.build_ts = function build_ts() {
  return src(['src/**/*.ts'], {
    base: './src',
  })
    .pipe(sourcemaps.init())
    .pipe(tsProject())
    .js.pipe(sourcemaps.write())
    .pipe(dest('app/'))
}

// 转译 scss 到 css
exports.build_scss = function build_scss() {
  return src(['src/**/*.scss'], {
    base: './src',
  })
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(dest('app/'))
}

// 定义默认任务
const defaultTask = series(this.copyStatic, this.build_ts, this.build_scss)
exports.default = defaultTask

// 定义监视任务
exports.auto = function auto() {
  return watch('src/**', { ignoreInitial: false }, defaultTask)
}
```

## 搭建 jest 测试环境

### 安装 jest

```shell
npm i -D jest ts-jest @types/jest
```

- [用 jest 测试 ts 文件](https://jestjs.io/docs/zh-Hans/getting-started#%E4%BD%BF%E7%94%A8-typescript)
- [ts-jest](https://kulshekhar.github.io/ts-jest/)

### 配置 jest

```shell
# 该命令可以生成一个支持 ts 文件测试的基本配置
npx ts-jest config:init
```

jest.config.js

```js
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  verbose: true,
  collectCoverage: true,
  coverageReporters: ['html', 'text'],
  testMatch: [
    '**/__tests__/**/*.[jt]s?(x)',
    '<rootDir>/tests/unit/**/*.(spec|test).[jt]s?(x)',
  ],
}
```

## 安装 electron 环境

```shell
npm i -D electron devtron
```

## 功能设计

添加 src/views/index.html, src/index.scss, src/main.ts, src/renderer.ts 等文件，实现功能

## 设置命令

package.json

```json
{
  "scripts": {
    "test": "jest",
    "start": "set NODE_ENV=development&& npm run build && electron .",
    "build": "gulp",
    "watch": "gulp auto"
  }
}
```

## 打包

### 安装打包工具

```shell
npm i -D electron-builder
```

### 定义打包命令

package.json

```json
{
  "scripts": {
    "package": "npm run build && electron-builder"
  }
}
```

### 配置 electron-builder

可以在 package.json 中简单地配置 electron-builder，也可以在单独的文件中进行配置

package.json

```json
{
  "build": {
    "appId": "io.gitee.pish7.bookmarker",
    "productName": "bookmarker",
    "win": {
      "target": "portable"
    }
  }
}
```
