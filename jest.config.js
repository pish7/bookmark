module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  verbose: true,
  collectCoverage: true,
  coverageReporters: ['html', 'text'],
  testMatch: [
    '**/__tests__/**/*.[jt]s?(x)',
    '<rootDir>/tests/unit/**/*.(spec|test).[jt]s?(x)',
  ],
}
