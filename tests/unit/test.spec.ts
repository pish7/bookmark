import add from '../../src/test'

describe('test.ts', function() {
  test('add', () => {
    const result = add(1, 2)
    expect(result).toBe(3)
  })
})
