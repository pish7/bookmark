import BookMarker from '../../src/Record'

describe('Record.ts', function() {
  test('创建对象', () => {
    const record = new BookMarker.Record('http://www.google.com', '谷歌')
    expect(record.url).toBe('http://www.google.com')
    expect(record.title).toBe('谷歌')
  })
  test('序列化', () => {
    const record = new BookMarker.Record('http://www.google.com', '谷歌')
    let json: any = JSON.stringify(record)

    json = JSON.parse(json)
    expect(Object.keys(json).length).toBe(2)
    expect(json.url).toBe('http://www.google.com')
    expect(json.title).toBe('谷歌')
  })
})
