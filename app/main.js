"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const electron_1 = require("electron");
/**
 * 窗口句柄
 * @type {BrowserWindow|null}
 */
let win = null;
/**
 * 当前环境是否为开发环境
 * @const
 * @type {boolean}
 */
const isDevelopment = process.env.NODE_ENV == 'development';
/**
 * 创建窗口
 */
function createWindow() {
    // 创建一个窗口
    win = new electron_1.BrowserWindow({
        minWidth: 400,
        minHeight: 300,
        show: false,
        webPreferences: {
            nodeIntegration: true,
        },
    });
    // 装载界面文件
    win.loadFile(`${__dirname}/views/index.html`);
    // 窗口关闭
    win.on('closed', () => {
        win = null;
    });
    // 渲染进程第一次完成绘制
    win.on('ready-to-show', () => {
        win && win.show();
    });
}
// 全部窗口关闭时程序退出
electron_1.app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        electron_1.app.quit();
    }
});
electron_1.app.on('ready', () => {
    createWindow(); // 创建窗口
    // 如果运行在开发环境中，则打开开发工具
    if (isDevelopment) {
        require('devtron').install(); // 安装 devtron
        win && win.webContents.openDevTools(); // 打开开发者工具
    }
    else {
        electron_1.Menu.setApplicationMenu(null); // 移除菜单
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSx1Q0FBbUQ7QUFFbkQ7OztHQUdHO0FBQ0gsSUFBSSxHQUFHLEdBQXlCLElBQUksQ0FBQTtBQUVwQzs7OztHQUlHO0FBQ0gsTUFBTSxhQUFhLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLElBQUksYUFBYSxDQUFBO0FBRTNEOztHQUVHO0FBQ0gsU0FBUyxZQUFZO0lBQ25CLFNBQVM7SUFDVCxHQUFHLEdBQUcsSUFBSSx3QkFBYSxDQUFDO1FBQ3RCLFFBQVEsRUFBRSxHQUFHO1FBQ2IsU0FBUyxFQUFFLEdBQUc7UUFDZCxJQUFJLEVBQUUsS0FBSztRQUNYLGNBQWMsRUFBRTtZQUNkLGVBQWUsRUFBRSxJQUFJO1NBQ3RCO0tBQ0YsQ0FBQyxDQUFBO0lBRUYsU0FBUztJQUNULEdBQUcsQ0FBQyxRQUFRLENBQUMsR0FBRyxTQUFTLG1CQUFtQixDQUFDLENBQUE7SUFFN0MsT0FBTztJQUNQLEdBQUcsQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUFFLEdBQUcsRUFBRTtRQUNwQixHQUFHLEdBQUcsSUFBSSxDQUFBO0lBQ1osQ0FBQyxDQUFDLENBQUE7SUFFRixjQUFjO0lBQ2QsR0FBRyxDQUFDLEVBQUUsQ0FBQyxlQUFlLEVBQUUsR0FBRyxFQUFFO1FBQzNCLEdBQUcsSUFBSSxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUE7SUFDbkIsQ0FBQyxDQUFDLENBQUE7QUFDSixDQUFDO0FBRUQsY0FBYztBQUNkLGNBQUcsQ0FBQyxFQUFFLENBQUMsbUJBQW1CLEVBQUUsR0FBRyxFQUFFO0lBQy9CLElBQUksT0FBTyxDQUFDLFFBQVEsS0FBSyxRQUFRLEVBQUU7UUFDakMsY0FBRyxDQUFDLElBQUksRUFBRSxDQUFBO0tBQ1g7QUFDSCxDQUFDLENBQUMsQ0FBQTtBQUVGLGNBQUcsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLEdBQUcsRUFBRTtJQUNuQixZQUFZLEVBQUUsQ0FBQSxDQUFDLE9BQU87SUFFdEIscUJBQXFCO0lBQ3JCLElBQUksYUFBYSxFQUFFO1FBQ2pCLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQSxDQUFDLGFBQWE7UUFDMUMsR0FBRyxJQUFJLEdBQUcsQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFLENBQUEsQ0FBQyxVQUFVO0tBQ2pEO1NBQU07UUFDTCxlQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLENBQUEsQ0FBQyxPQUFPO0tBQ3RDO0FBQ0gsQ0FBQyxDQUFDLENBQUEiLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGFwcCwgQnJvd3NlcldpbmRvdywgTWVudSB9IGZyb20gJ2VsZWN0cm9uJ1xyXG5cclxuLyoqXHJcbiAqIOeql+WPo+WPpeafhFxyXG4gKiBAdHlwZSB7QnJvd3NlcldpbmRvd3xudWxsfVxyXG4gKi9cclxubGV0IHdpbjogQnJvd3NlcldpbmRvdyB8IG51bGwgPSBudWxsXHJcblxyXG4vKipcclxuICog5b2T5YmN546v5aKD5piv5ZCm5Li65byA5Y+R546v5aKDXHJcbiAqIEBjb25zdFxyXG4gKiBAdHlwZSB7Ym9vbGVhbn1cclxuICovXHJcbmNvbnN0IGlzRGV2ZWxvcG1lbnQgPSBwcm9jZXNzLmVudi5OT0RFX0VOViA9PSAnZGV2ZWxvcG1lbnQnXHJcblxyXG4vKipcclxuICog5Yib5bu656qX5Y+jXHJcbiAqL1xyXG5mdW5jdGlvbiBjcmVhdGVXaW5kb3coKSB7XHJcbiAgLy8g5Yib5bu65LiA5Liq56qX5Y+jXHJcbiAgd2luID0gbmV3IEJyb3dzZXJXaW5kb3coe1xyXG4gICAgbWluV2lkdGg6IDQwMCxcclxuICAgIG1pbkhlaWdodDogMzAwLFxyXG4gICAgc2hvdzogZmFsc2UsXHJcbiAgICB3ZWJQcmVmZXJlbmNlczoge1xyXG4gICAgICBub2RlSW50ZWdyYXRpb246IHRydWUsXHJcbiAgICB9LFxyXG4gIH0pXHJcblxyXG4gIC8vIOijhei9veeVjOmdouaWh+S7tlxyXG4gIHdpbi5sb2FkRmlsZShgJHtfX2Rpcm5hbWV9L3ZpZXdzL2luZGV4Lmh0bWxgKVxyXG5cclxuICAvLyDnqpflj6PlhbPpl61cclxuICB3aW4ub24oJ2Nsb3NlZCcsICgpID0+IHtcclxuICAgIHdpbiA9IG51bGxcclxuICB9KVxyXG5cclxuICAvLyDmuLLmn5Pov5vnqIvnrKzkuIDmrKHlrozmiJDnu5jliLZcclxuICB3aW4ub24oJ3JlYWR5LXRvLXNob3cnLCAoKSA9PiB7XHJcbiAgICB3aW4gJiYgd2luLnNob3coKVxyXG4gIH0pXHJcbn1cclxuXHJcbi8vIOWFqOmDqOeql+WPo+WFs+mXreaXtueoi+W6j+mAgOWHulxyXG5hcHAub24oJ3dpbmRvdy1hbGwtY2xvc2VkJywgKCkgPT4ge1xyXG4gIGlmIChwcm9jZXNzLnBsYXRmb3JtICE9PSAnZGFyd2luJykge1xyXG4gICAgYXBwLnF1aXQoKVxyXG4gIH1cclxufSlcclxuXHJcbmFwcC5vbigncmVhZHknLCAoKSA9PiB7XHJcbiAgY3JlYXRlV2luZG93KCkgLy8g5Yib5bu656qX5Y+jXHJcblxyXG4gIC8vIOWmguaenOi/kOihjOWcqOW8gOWPkeeOr+Wig+S4re+8jOWImeaJk+W8gOW8gOWPkeW3peWFt1xyXG4gIGlmIChpc0RldmVsb3BtZW50KSB7XHJcbiAgICByZXF1aXJlKCdkZXZ0cm9uJykuaW5zdGFsbCgpIC8vIOWuieijhSBkZXZ0cm9uXHJcbiAgICB3aW4gJiYgd2luLndlYkNvbnRlbnRzLm9wZW5EZXZUb29scygpIC8vIOaJk+W8gOW8gOWPkeiAheW3peWFt1xyXG4gIH0gZWxzZSB7XHJcbiAgICBNZW51LnNldEFwcGxpY2F0aW9uTWVudShudWxsKSAvLyDnp7vpmaToj5zljZVcclxuICB9XHJcbn0pXHJcbiJdfQ==
