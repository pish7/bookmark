"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BookMarker;
(function (BookMarker) {
    /**
     * 一个记录
     * @class
     */
    class Record {
        /**
         * 创建一个记录
         * @param url 网址
         * @param title 网站标题
         */
        constructor(url, title) {
            this.url = url;
            this.title = title;
        }
        // 序列化
        toJSON() {
            return {
                url: this.url,
                title: this.title,
            };
        }
    }
    BookMarker.Record = Record;
})(BookMarker || (BookMarker = {}));
exports.default = BookMarker;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIlJlY29yZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLElBQVUsVUFBVSxDQXFCbkI7QUFyQkQsV0FBVSxVQUFVO0lBQ2xCOzs7T0FHRztJQUNILE1BQWEsTUFBTTtRQUNqQjs7OztXQUlHO1FBQ0gsWUFBbUIsR0FBVyxFQUFTLEtBQWE7WUFBakMsUUFBRyxHQUFILEdBQUcsQ0FBUTtZQUFTLFVBQUssR0FBTCxLQUFLLENBQVE7UUFBRyxDQUFDO1FBRXhELE1BQU07UUFDTixNQUFNO1lBQ0osT0FBTztnQkFDTCxHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUc7Z0JBQ2IsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO2FBQ2xCLENBQUE7UUFDSCxDQUFDO0tBQ0Y7SUFmWSxpQkFBTSxTQWVsQixDQUFBO0FBQ0gsQ0FBQyxFQXJCUyxVQUFVLEtBQVYsVUFBVSxRQXFCbkI7QUFFRCxrQkFBZSxVQUFVLENBQUEiLCJmaWxlIjoiUmVjb3JkLmpzIiwic291cmNlc0NvbnRlbnQiOlsibmFtZXNwYWNlIEJvb2tNYXJrZXIge1xyXG4gIC8qKlxyXG4gICAqIOS4gOS4quiusOW9lVxyXG4gICAqIEBjbGFzc1xyXG4gICAqL1xyXG4gIGV4cG9ydCBjbGFzcyBSZWNvcmQge1xyXG4gICAgLyoqXHJcbiAgICAgKiDliJvlu7rkuIDkuKrorrDlvZVcclxuICAgICAqIEBwYXJhbSB1cmwg572R5Z2AXHJcbiAgICAgKiBAcGFyYW0gdGl0bGUg572R56uZ5qCH6aKYXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyB1cmw6IHN0cmluZywgcHVibGljIHRpdGxlOiBzdHJpbmcpIHt9XHJcblxyXG4gICAgLy8g5bqP5YiX5YyWXHJcbiAgICB0b0pTT04oKSB7XHJcbiAgICAgIHJldHVybiB7XHJcbiAgICAgICAgdXJsOiB0aGlzLnVybCxcclxuICAgICAgICB0aXRsZTogdGhpcy50aXRsZSxcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgQm9va01hcmtlclxyXG4iXX0=
